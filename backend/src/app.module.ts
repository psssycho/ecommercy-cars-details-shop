import { Module } from '@nestjs/common';
import { UsersModule } from './modules/users/users.module';
import { PrismaService } from './prisma/prisma.service';
import { ProductsModule } from './modules/products/products.module';
import { PrismaModule } from './prisma/prisma.module';

@Module({
  imports: [UsersModule, ProductsModule, PrismaModule],
  controllers: [],
  providers: [PrismaService],
})
export class AppModule {}
