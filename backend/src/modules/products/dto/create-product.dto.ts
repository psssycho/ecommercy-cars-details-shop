export class CreateProductDto {
  id: number;
  name: string;
  description: string;
  image: string;
  category: string;
  price: number;
  authToken: string;
}
