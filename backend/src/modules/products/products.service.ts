import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class ProductsService {
  constructor(private prisma: PrismaService) {}

  async create(createProductDto: CreateProductDto) {
    const { authToken, ...dataWithoutAuthToken } = createProductDto;
    return await this.prisma.product.create({
      data: {
        ...dataWithoutAuthToken,
        price: Number(dataWithoutAuthToken.price),
      },
    });
  }

  async findAll() {
    return await this.prisma.product.findMany();
  }

  async findOne(id: number) {
    return await this.prisma.product.findFirst({ where: { id: id } });
  }

  remove(id: number) {
    this.prisma.product.delete({ where: { id: id } });
  }
}
