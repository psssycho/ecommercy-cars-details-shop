import {
  BadRequestException,
  ConflictException,
  Injectable,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import * as bcrypt from 'bcrypt';
import { LoginUserDto } from './dto/login-user.dto';
import { response } from 'express';

@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService) {}

  async login(loginUserDto: LoginUserDto) {
    const existingUser = await this.prisma.users.findFirst({
      where: { email: loginUserDto.email },
    });

    if (!existingUser) {
      throw new BadRequestException('Can`t find this user');
    }

    if (!(await bcrypt.compare(loginUserDto.password, existingUser.password))) {
      throw new BadRequestException('Invalid password');
    }
    return existingUser;
  }

  async create(createUserDto: CreateUserDto) {
    const existingUser = await this.prisma.users.findFirst({
      where: { email: createUserDto.email },
    });

    if (existingUser) {
      throw new ConflictException('User already exists');
    }

    const hashedPassword = await bcrypt.hash(createUserDto.password, 10);
    const newUser = await this.prisma.users.create({
      data: {
        email: createUserDto.email,
        password: hashedPassword,
        firstname: createUserDto.firstname,
        lastname: createUserDto.lastname,
      },
    });

    return newUser;
  }

  async findAll() {
    return await this.prisma.users.findMany();
  }

  async findOne(id: number) {
    return await this.prisma.users.findFirst({ where: { id: id } });
  }
}
