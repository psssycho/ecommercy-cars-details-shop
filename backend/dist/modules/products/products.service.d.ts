import { CreateProductDto } from './dto/create-product.dto';
import { PrismaService } from 'src/prisma/prisma.service';
export declare class ProductsService {
    private prisma;
    constructor(prisma: PrismaService);
    create(createProductDto: CreateProductDto): Promise<{
        id: number;
        name: string;
        description: string;
        image: string;
        category: string;
        price: number;
    }>;
    findAll(): Promise<{
        id: number;
        name: string;
        description: string;
        image: string;
        category: string;
        price: number;
    }[]>;
    findOne(id: number): Promise<{
        id: number;
        name: string;
        description: string;
        image: string;
        category: string;
        price: number;
    }>;
    remove(id: number): void;
}
