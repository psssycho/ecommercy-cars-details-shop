import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
export declare class ProductsController {
    private readonly productsService;
    constructor(productsService: ProductsService);
    create(createProductDto: CreateProductDto): Promise<{
        id: number;
        name: string;
        description: string;
        image: string;
        category: string;
        price: number;
    }>;
    findAll(): Promise<{
        id: number;
        name: string;
        description: string;
        image: string;
        category: string;
        price: number;
    }[]>;
    findOne(id: string): Promise<{
        id: number;
        name: string;
        description: string;
        image: string;
        category: string;
        price: number;
    }>;
    remove(id: string): void;
}
