import { CreateUserDto } from './dto/create-user.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { LoginUserDto } from './dto/login-user.dto';
export declare class UsersService {
    private prisma;
    constructor(prisma: PrismaService);
    login(loginUserDto: LoginUserDto): Promise<{
        id: number;
        firstname: string;
        lastname: string;
        email: string;
        password: string;
    }>;
    create(createUserDto: CreateUserDto): Promise<{
        id: number;
        firstname: string;
        lastname: string;
        email: string;
        password: string;
    }>;
    findAll(): Promise<{
        id: number;
        firstname: string;
        lastname: string;
        email: string;
        password: string;
    }[]>;
    findOne(id: number): Promise<{
        id: number;
        firstname: string;
        lastname: string;
        email: string;
        password: string;
    }>;
}
