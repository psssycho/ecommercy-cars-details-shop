import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    login(loginUserDto: LoginUserDto): Promise<{
        id: number;
        firstname: string;
        lastname: string;
        email: string;
        password: string;
    }>;
    create(createUserDto: CreateUserDto): Promise<{
        id: number;
        firstname: string;
        lastname: string;
        email: string;
        password: string;
    }>;
    findAll(): Promise<{
        id: number;
        firstname: string;
        lastname: string;
        email: string;
        password: string;
    }[]>;
    findOne(id: string): Promise<{
        id: number;
        firstname: string;
        lastname: string;
        email: string;
        password: string;
    }>;
}
