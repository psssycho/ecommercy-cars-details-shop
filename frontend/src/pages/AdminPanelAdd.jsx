import { React, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import { Navbar } from '../components';

const AdminPanelAdd = () => {
	const navigate = useNavigate();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [image, setImage] = useState('');
	const [category, setCategory] = useState('');

	const handleAddProduct = async e => {
		e.preventDefault();

		const authToken = localStorage.getItem('authToken');
		const postData = {
			name,
			description,
			price,
			image,
			category,
			authToken,
		};

		try {
			const response = await fetch('http://localhost:3000/api/v1/products', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(postData),
			});

			if (response.status === 201) {
				console.log('Success');
				toast.success('Product added.');
			} else {
				console.error('Request failed');
			}
		} catch (error) {
			console.error('Error during request', error);
		}
	};
	return (
		<>
			<Navbar />
			<div className='container my-3 py-3'>
				<h1 className='text-center'>Добавление товара</h1>
				<ToastContainer
					position='bottom-right'
					autoClose={5000}
					hideProgressBar={false}
					newestOnTop={false}
					closeOnClick
					rtl={false}
					pauseOnFocusLoss
					draggable
					pauseOnHover
					theme='dark'
				/>
				<div class='row my-4 h-100'>
					<div className='col-md-4 col-lg-4 col-sm-8 mx-auto'>
						<form onSubmit={handleAddProduct}>
							<div class='form my-3'>
								<label for='Email'>Имя товара</label>
								<input
									class='form-control'
									id='Name'
									value={name}
									placeholder='имя_товара'
									onChange={e => setName(e.target.value)}
								/>
							</div>
							<div class='form  my-3'>
								<label for='Password'>Описание</label>
								<input
									class='form-control'
									id='Description'
									value={description}
									placeholder='описание'
									onChange={e => setDescription(e.target.value)}
								/>
							</div>
							<div class='form my-3'>
								<label for='Firstname'>Категория</label>
								<input
									class='form-control'
									id='Category'
									value={category}
									placeholder='Категория товара'
									onChange={e => setCategory(e.target.value)}
								/>
							</div>
							<div class='form my-3'>
								<label for='Lastname'>URL изображения</label>
								<input
									class='form-control'
									id='image'
									value={image}
									placeholder='Введите URL изображения'
									onChange={e => setImage(e.target.value)}
								/>
							</div>
							<div class='form my-3'>
								<label for='Phone'>Цена</label>
								<input
									class='form-control'
									value={price}
									placeholder='Введите цену товара'
									onChange={e => setPrice(e.target.value)}
								/>
							</div>
							<div className='text-center'>
								<button class='my-2 mx-auto btn btn-dark' type='submit' enable>
									Добавить товар
								</button>
							</div>
							<div className='text-center'>
								<button class='my-2 mx-auto btn btn-dark' type='submit' enable>
									<Link to='/admin' className='text-decoration-none text-info'>
										Вернуться в меню
									</Link>{' '}
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</>
	);
};

export default AdminPanelAdd;
