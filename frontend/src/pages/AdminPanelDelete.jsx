import { React, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import { Navbar } from '../components';

const AdminPanelDelete = () => {
	const navigate = useNavigate();
	const [name, setName] = useState('');

	const handleDeleteProduct = async e => {
		e.preventDefault();

		const authToken = localStorage.getItem('authToken');
		const postData = {
			name,
			authToken,
		};

		try {
			const response = await fetch(
				`http://localhost:3000/api/v1/product/delete`,
				{
					method: 'DELETE',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(postData),
				}
			);

			if (response.status === 200) {
				console.log('Success');
				toast.success('Product deleted');
			} else {
				console.error('Request failed');
				toast.error('Fail');
			}
		} catch (error) {
			console.error('Error during request', error);
		}
	};
	return (
		<>
			<Navbar />
			<div className='container my-3 py-3'>
				<h1 className='text-center'>Удаление товара</h1>
				<ToastContainer
					position='bottom-right'
					autoClose={5000}
					hideProgressBar={false}
					newestOnTop={false}
					closeOnClick
					rtl={false}
					pauseOnFocusLoss
					draggable
					pauseOnHover
					theme='dark'
				/>
				<div class='row my-4 h-100'>
					<div className='col-md-4 col-lg-4 col-sm-8 mx-auto'>
						<form onSubmit={handleDeleteProduct}>
							<div class='form my-3'>
								<label for='Email'>Имя товара</label>
								<input
									class='form-control'
									id='Name'
									value={name}
									placeholder='Имя товара'
									onChange={e => setName(e.target.value)}
								/>
							</div>
							<div className='text-center'>
								<button class='my-2 mx-auto btn btn-dark' type='submit' enable>
									Удалить товар
								</button>
							</div>
							<div className='text-center'>
								<button class='my-2 mx-auto btn btn-dark' type='submit' enable>
									<Link to='/admin' className='text-decoration-none text-info'>
										Вернуться в меню
									</Link>{' '}
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</>
	);
};

export default AdminPanelDelete;
