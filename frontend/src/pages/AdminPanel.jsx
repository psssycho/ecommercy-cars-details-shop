import { React } from 'react';
import { Link } from 'react-router-dom';
import { Navbar } from '../components';

const AdminPanel = () => {
	return (
		<>
			<Navbar />
			<div className='container my-3 py-3'>
				<h1 className='text-center'>Работа с товарами</h1>
				<div className='text-center'>
					<button class='my-2 mx-auto btn btn-dark' type='submit' enable>
						<Link to='/admin/delete' className='text-decoration-none text-info'>
							Удалить товар
						</Link>{' '}
					</button>
				</div>
				<div className='text-center'>
					<button class='my-2 mx-auto btn btn-dark' type='submit' enable>
						<Link to='/admin/add' className='text-decoration-none text-info'>
							Добавить товар
						</Link>{' '}
					</button>
				</div>
			</div>
		</>
	);
};

export default AdminPanel;
