import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Navbar } from '../components';

const Login = () => {
	const navigate = useNavigate();
	const [email, setEmail] = useState('');
	const [admin, setAdmin] = useState('');
	const [password, setPassword] = useState('');

	const handleLogin = async e => {
		e.preventDefault();

		console.log(
			JSON.stringify({
				email,
				password,
			})
		);

		const response = await fetch('http://localhost:3000/api/v1/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email,
				password,
			}),
		});

		if (response.status === 200) {
			const data = await response.json();
			if (email === 'moseyarthas@gmail.com') {
				localStorage.setItem('authToken', 'mock_authToken_admin');
			} else {
				localStorage.setItem('authToken', 'mock_authToken_user');
			}
			navigate('/');
		} else {
			toast.error('Incorrect data.');
		}
	};

	return (
		<>
			<Navbar />
			<div className='container my-3 py-3'>
				<h1 className='text-center'>Логин</h1>
				<div class='row my-4 h-100'>
					<div className='col-md-4 col-lg-4 col-sm-8 mx-auto'>
						<form onSubmit={handleLogin}>
							<div className='my-3'>
								<label htmlFor='floatingInput'>Email</label>
								<input
									type='email'
									className='form-control'
									id='floatingInput'
									placeholder='name@example.com'
									value={email}
									onChange={e => setEmail(e.target.value)}
								/>
							</div>
							<div className='my-3'>
								<label htmlFor='floatingPassword'>Пароль</label>
								<input
									type='password'
									className='form-control'
									id='floatingPassword'
									placeholder='Ваш пароль'
									value={password}
									onChange={e => setPassword(e.target.value)}
								/>
							</div>
							<ToastContainer
								position='bottom-right'
								autoClose={5000}
								hideProgressBar={false}
								newestOnTop={false}
								closeOnClick
								rtl={false}
								pauseOnFocusLoss
								draggable
								pauseOnHover
								theme='dark'
							/>
							<div className='my-3'>
								<p>
									Впервые здесь?{' '}
									<Link
										to='/register'
										className='text-decoration-underline text-info'
									>
										Регистрация
									</Link>{' '}
								</p>
							</div>
							<div className='text-center'>
								<button className='my-2 mx-auto btn btn-dark' type='submit'>
									Вход
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</>
	);
};

export default Login;
