import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import store from './redux/store';

import {
	AboutPage,
	AdminPanel,
	AdminPanelAdd,
	AdminPanelDelete,
	Cart,
	Checkout,
	ContactPage,
	Home,
	Login,
	PageNotFound,
	Product,
	Products,
	Register,
} from './pages';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
	<BrowserRouter>
		<Provider store={store}>
			<Routes>
				<Route path='/' element={<Home />} />
				<Route path='/product' element={<Products />} />
				<Route path='/product/:id' element={<Product />} />
				<Route path='/about' element={<AboutPage />} />
				<Route path='/contact' element={<ContactPage />} />
				<Route path='/cart' element={<Cart />} />
				<Route path='/login' element={<Login />} />
				<Route path='/register' element={<Register />} />
				<Route path='/checkout' element={<Checkout />} />
				<Route path='*' element={<PageNotFound />} />
				<Route path='/product/*' element={<PageNotFound />} />
				<Route path='/admin' element={<AdminPanel />} />
				<Route path='/admin/add' element={<AdminPanelAdd />} />
				<Route path='/admin/delete' element={<AdminPanelDelete />} />
			</Routes>
		</Provider>
	</BrowserRouter>
);
